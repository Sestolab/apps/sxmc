#include <stdio.h>
#include <stdlib.h>
#include <xcb/xcb.h>
#include <xcb/xtest.h>


xcb_button_index_t char_to_mouse_button(char *a, int *i)
{
	switch (a[++*i])
	{
		case '1': return XCB_BUTTON_INDEX_1;
		case '2': return XCB_BUTTON_INDEX_2;
		case '3': return XCB_BUTTON_INDEX_3;
		case '4': return XCB_BUTTON_INDEX_4;
		case '5': return XCB_BUTTON_INDEX_5;
		default: --*i; return XCB_BUTTON_INDEX_1;
	}
}

int16_t arg_to_int(char *a, int *i)
{
	int16_t result = 0;
	char sign = '\0';

	++*i;

	if (a[*i] == '-' || a[*i] == '+')
	{
		sign = a[*i];
		++*i;
	}

	while (a[*i] >= '0' && a[*i] <= '9')
	{
		result = result * 10 + a[*i] - '0';
		++*i;
	}

	--*i;

	return sign == '-' ? -result : result;
}


int main(int argc, char *argv[])
{
	xcb_connection_t *c = xcb_connect(NULL, NULL);

	if (xcb_connection_has_error(c))
	{
		fprintf(stderr, "Could not connect to the X server.\n");
		return 1;
	}

	xcb_screen_t *s = xcb_setup_roots_iterator(xcb_get_setup(c)).data;
	xcb_query_pointer_reply_t *pointer = xcb_query_pointer_reply(c, xcb_query_pointer(c, s->root), NULL);

	if (argc == 1) printf("%d %d", pointer->root_x, pointer->root_y);
	else for (int i = 0; argv[1][i] != '\0'; ++i) switch (argv[1][i])
	{
		case 'p':
			xcb_test_fake_input(c, XCB_BUTTON_PRESS, char_to_mouse_button(argv[1], &i),
				XCB_CURRENT_TIME, XCB_NONE, 0, 0, 0);
			xcb_flush(c);
			break;
		case 'r':
			xcb_test_fake_input(c, XCB_BUTTON_RELEASE, char_to_mouse_button(argv[1], &i),
				XCB_CURRENT_TIME, XCB_NONE, 0, 0, 0);
			xcb_flush(c);
			break;
		case 'x':
			if (argv[1][i+1] == '-' || argv[1][i+1] == '+')
				xcb_warp_pointer(c, XCB_NONE, XCB_NONE, 0, 0, 0, 0, arg_to_int(argv[1], &i), 0);
			else
				xcb_warp_pointer(c, XCB_NONE, s->root, 0, 0, 0, 0, arg_to_int(argv[1], &i), pointer->root_y);
			pointer = xcb_query_pointer_reply(c, xcb_query_pointer(c, s->root), NULL);
			xcb_flush(c);
			break;
		case 'y':
			if (argv[1][i+1] == '-' || argv[1][i+1] == '+')
				xcb_warp_pointer(c, XCB_NONE, XCB_NONE, 0, 0, 0, 0, 0, arg_to_int(argv[1], &i));
			else
				xcb_warp_pointer(c, XCB_NONE, s->root, 0, 0, 0, 0, pointer->root_x, arg_to_int(argv[1], &i));
			pointer = xcb_query_pointer_reply(c, xcb_query_pointer(c, s->root), NULL);
			xcb_flush(c);
			break;
		default: break;
	}

	free(pointer);

	xcb_flush(c);
	xcb_disconnect(c);
	return 0;
}

