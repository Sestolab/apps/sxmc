# sxmc

Simple X Mouse Controller.

## Dependencies

* libxcb

## Installation

* $ make
* # make install

## Commands

* `p`/`r` - press/release the mouse button
	* `1` - LEFT
	* `2` - MIDDLE
	* `3` - RIGHT
	* `4` - WHEEL UP
	* `5` - WHEEL DOWN
* `x`/`y` - change x/y cursor position
	* `+` - add to the current cursor position
	* `-` - subtract from the current cursor position

## Examples

Show the current cursor position:

```
$ sxmc
```

Click the LEFT mouse button:

```
$ sxmc pr
$ sxmc p1r1
```

Double-click LEFT mouse button:

```
$ sxmc prpr
$ sxmc p1r1p1r1
```

Click the MIDDLE mouse button:

```
$ sxmc p2r2
```

Click the RIGHT mouse button:

```
$ sxmc p3r3
```

Set cursor position:

```
$ sxmc x100
$ sxmc y100
$ sxmc x100y100
```

Add to the current cursor position:

```
$ sxmc x+100
$ sxmc y+100
$ sxmc x+100y+100
```

Subtract from the current cursor position:

```
$ sxmc x-100
$ sxmc y-100
$ sxmc x-100y-100
```

