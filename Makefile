CFLAGS += -Wall -Wextra -pedantic -lxcb -lxcb-xtest

PREFIX ?= /usr/local
CC ?= cc

all: sxmc

sxmc: sxmc.c
	$(CC) sxmc.c $(CFLAGS) -o sxmc

install: sxmc
	install -Dm755 sxmc -t $(DESTDIR)$(PREFIX)/bin

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/sxmc

clean:
	rm -f sxmc

.PHONY: all install uninstall clean

